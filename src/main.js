// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import $ from 'jquery';
import api from './api';
import JsonViewer from 'vue-json-viewer';
import './assets/static/css/aliicon.css';
import router from './router';

// import VueSocketio from 'vue-socket.io';
// import socketio from 'socket.io-client';



window.$ = $;
Vue.prototype.$api = api;

var echarts = require('echarts');

Vue.use(ElementUI);
Vue.use(JsonViewer);


// Vue.use(VueSocketio,socketio('http://127.0.0.1:5000'))

// Vue.use(new VueSocketio({
//   debug: false,
//   connection: socketio('http://127.0.0.1:5000/'),
//   }))

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
