import base from './base'; // 导入接口域名列表
import http from '../components/httpapi' // 导入http中创建的axios实例

const getIndexInfo = {  
    GetUserInfo (data) {     
        return http.post(`${base}/login`, data);    
    },
    getIndexPageData(){
        return http.get(`${base}/index`);   
    }
}

export default getIndexInfo;