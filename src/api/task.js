import base from './base'; // 导入接口域名列表
import http from '../components/httpapi' // 导入http中创建的axios实例

const taskApi = {
    // 获取定时列表
    GetTaskList(data){
        return http.post(`${base}/taskmanager/show`,data)
    },
    // 添加定时任务
    AddTask(data){
        return http.post(`${base}/taskmanager/add`,data)
    },
    // 操作定时任务
    TaskOpreat(data){
        return http.post(`${base}/taskmanager/taskop`,data)
    },

    // 编辑定时任务页面获取任务资料
    GetJobInfo(params){
        return http.get(`${base}/taskmanager/getjobinfo`,{params:params})
    },
    // 编辑任务
    TaskEdit(data){
        return http.post(`${base}/taskmanager/edit`,data)
    },
    // 获取所有的项目和所有的脚本
    GetAllProjectData(){
        return http.get(`${base}/addtask/apdata`)
    },

    // 删除任务
    DeleteTask(data){
        return http.post(`${base}/taskmanager/del`,data)
    },

    // 获取项目下面所有的环境
    GetProjectEnvs(params){
        return http.get(`${base}/edittask/getenvs`,{params:params})
    }



}


export default taskApi;