/**
 * 接口域名的管理
 */
// const base = {    
//     development: 'http://127.0.0.1:5000',    
//     production: 'http://www.qixiubao.club'
// }

let base = "";
switch (process.env.NODE_ENV) {
    case 'development':
        base = "http://127.0.0.1:5000"  //开发环境url
        break
    case 'production':
        base = "http://192.168.0.210:10086"   //生产环境url
        break
}

export default base;