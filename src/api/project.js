import base from './base'; // 导入接口域名列表
import http from '../components/httpapi' // 导入http中创建的axios实例

const projectApi = {  
    GetAllProject () {     
        return http.get(`${base}/projects`);    
    },
    AddProject(data){
        return http.post(`${base}/projects/add`,data);   
    },
    EditProject(data){
        return http.post(`${base}/projects/edit`,data); 
    },
    DeleteProject(data){
        return http.post(`${base}/projects/del`,data); 
    }
}

export default projectApi;