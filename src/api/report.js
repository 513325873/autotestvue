import base from './base'; // 导入接口域名列表
import http from '../components/httpapi' // 导入http中创建的axios实例

const reportApi = {
    // 获取项目和脚本
    GetProjectAndTc(){
        return http.get(`${base}/reprot/getprotc`)
    },
    // 获取测试报告
    GetReportList(data){
        return http.post(`${base}/reprot/list`,data)
    },
    // 获取报告详情
    GetReportDetail(data){
        return http.post(`${base}/reprot/show`,data)
    },
    // 删除报告
    DeleteReport(data){
        return http.post(`${base}/reprot/del`,data)
    }
}

export default reportApi;