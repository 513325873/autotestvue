import base from './base'; // 导入接口域名列表
import http from '../components/httpapi' // 导入http中创建的axios实例

const testCaseApi = {
    // 获取测试用例列表
    GetTestCase(data){
        return http.post(`${base}/testcases`,data)
    },
    // 运行测试用例
    RunTest(data){
        return http.post(`${base}/testcases/runcases`,data)
    },
    // 添加测试用例名字
    AddTestName(data){
        return http.post(`${base}/testcases/addtestname`,data)
    },
    // 添加脚本用例
    AddScriptTestCase(data){
        return http.post(`${base}/testcases/add/script`,data)
    },
    // 添加Model信息
    AddModelName(data){
        return http.post(`${base}/testcases/add/model`,data)
    },
    //添加API步骤信息
    AddApiStepName(data){
        return http.post(`${base}/testcases/api/add/step`,data)
    },
    //编辑API步骤信息
    EditApiStepName(data){
        return http.post(`${base}/testcases/api/edit/step`,data)
    },

    //添加UI步骤信息
    AddUiStepName(data){
        return http.post(`${base}/testcases/ui/add/step`,data)
    },

    //编辑UI步骤信息
    EditUiStepName(data){
        return http.post(`${base}/testcases/ui/edit/step`,data)
    },


    // 添加UI测试用例
    AddUiTestCase(data){
        return http.post(`${base}/testcases/add/ui`,data)
    },
    // 获取树形信息
    GetTreeJson(data){
        return http.post(`${base}/testcases/caseinfo`,data)
    },
    // 更改测试用例的Model信息
    EditModelName(data){
        return http.post(`${base}/testcases/edit/model`,data)
    },
    // 删除测试用例
    DeleteTestCase(data){
        return http.post(`${base}/testcases/delsript`,data)
    },
    // 删除测试点信息
    DelSetpInfo(data){
        return http.post(`${base}/testcases/del/delinfo`,data)
    },
    DebugApiInfo(data){
        return http.post(`${base}/testcases/debug/api`,data)
    },
    // 获取所有数据
    GetAllData(params){
        return http.get(`${base}/testcases/alldata`,{params:params})
    },
    // 获取测试用例信息
    GetSetpCaseInfo(data){
        return http.post(`${base}/testcases/casesetpinfo`,data)
    },
    // 设置全局header
    GlobalHeaderSetting(data){
        return http.post(`${base}/testcases/globalheader`,data)
    },
    // 设置全局用户自定义变量
    GlobalVarSetting(data){
        return http.post(`${base}/testcases/globalvariable`,data)
    },
    // 下载测试样例
    DownLoadSample(params){
        return http.get(`${base}/testcases/download/sample`,{params:params})
    },
    // 获取接口？后面的数据自动填充
    GetUrlParams(data){
        return http.post(`${base}/testcases/getparams`,data)
    },
    // 获取测试用例名字
    GetTestCaseName(params){
        return http.get(`${base}/testcases/gettestcasename`,{params:params})
    },

    // 交换tree节点
    ChangePosition(data){
        return http.post(`${base}/testcases/changeposition`,data)
    },

    //复制添加脚本的数据
    CopyTestSuite(data){
        return http.post(`${base}/testcases/copysuite`,data)
    },

    // 获取操作记录数据
    GetRecordData(params){
        return http.get(`${base}/testcases/opreationrecord`,{params:params})
    },

}

export default testCaseApi;