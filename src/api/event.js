import base from './base'; // 导入接口域名列表
import http from '../components/httpapi' // 导入http中创建的axios实例

const eventApi = {  
    GetEvents(data) {     
        return http.post(`${base}/event/show`,data);    
    },
    AddEvents(data){
        return http.post(`${base}/event/add`,data);   
    },
    EditEvents(data){
        return http.post(`${base}/events/edit`,data); 
    },
    DeleteEvents(data){
        return http.post(`${base}/events/del`,data); 
    }
}

export default eventApi;