/**
 * article模块接口列表
 */

import base from './base'; // 导入接口域名列表
import http from '../components/httpapi' // 导入http中创建的axios实例

const login = {  
    login (data) {     
        console.log("baseURL",base)
        return http.post(`${base}/login`, data);    
    },
    logout(){
        return http.get(`${base}/logout`)
    }
}

export default login;