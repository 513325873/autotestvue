/** 
 * api接口的统一出口
 */

import login from '@/api/login';
import webindex from '@/api/webIndex';
import project from '@/api/project';
import event from '@/api/event';
import testcase from '@/api/testcase';
import task from '@/api/task';
import report from '@/api/report';
import usermanager from '@/api/usermanager';
// 其他模块的接口……

// 导出接口
export default {    
    login,
    webindex,
    project,
    event,
    testcase,
    task,
    report,
    usermanager,
    // ……
}