import base from './base'; // 导入接口域名列表
import http from '../components/httpapi' // 导入http中创建的axios实例

const UserManagerApi = {
    // 获取用户列表
    GetUserList(){
        return http.get(`${base}/userManager`)
    },
    // 添加用户
    AddUser(data){
        return http.post(`${base}/userManager/add`,data)
    },
    // 操作用户
    // 1：删除2：冻结3：解冻
    OpreatUser(data){
        return http.post(`${base}/userManager/change`,data)
    },
    // 编辑用户
    EditUser(data){
        return http.post(`${base}/userManager/edit`,data)
    }
}

export default UserManagerApi;