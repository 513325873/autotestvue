import Vue from 'vue'
import Router from 'vue-router'
import login from '@/pages/login.vue'
import Back from '@/components/Back.vue'
import Indexs from '@/pages/index.vue'
import test from '@/pages/test.vue'
import Project from '@/pages/project.vue'
import Events from '@/pages/event.vue'
import TestSuite from '@/pages/testsuite.vue'
import AddTestCase from '@/pages/addtestcase.vue'
import AddScriptPage from '@/pages/addscriptpage.vue'
import ApiInfo from '@/pages/apiinfo.vue'
import UiInfo from '@/pages/uiinfo.vue'
import Task from '@/pages/task.vue'
import CreatTask from '@/pages/creattask.vue'
import EditTask from '@/pages/edittask.vue'
import ReportList from '@/pages/testcasereport.vue'
import ReportDetail from '@/pages/reportdetail.vue'
import UserManager from '@/pages/usermanager.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Back,
      children: [
        {
          path:"/test",
          name: 'test',
          component: test
        },
        {
          path: '',
          name: 'Indexs',
          component: Indexs
        },
        {
          path: 'index',
          name: 'Indexs',
          component: Indexs
        },
        {
          path: 'event',
          name: 'Event',
          component: Events
        },
        {
          path: 'testcase',
          name: 'TestSuite',
          component: TestSuite
        },
        {
          path: 'addcasepage',
          name: 'AddTestCase',
          component: AddTestCase
        },
        {
          path: 'addscriptpage',
          name: 'AddScriptPage',
          component: AddScriptPage
        },
        {
          path: 'apiinfo',
          name: 'ApiInfo',
          component: ApiInfo
        },
        {
          path: 'uiinfo',
          name: 'UiInfo',
          component: UiInfo
        },
        {
          path: 'task',
          name: 'Task',
          component: Task
        },
        {
          path: 'creattask',
          name: 'CreatTask',
          component: CreatTask
        },
        {
          path: 'edittask',
          name: 'EditTask',
          component: EditTask
        },
        {
          path: 'reportlist',
          name: 'ReportList',
          component: ReportList
        },
        {
          path: 'reportdetail',
          name: 'ReportDetail',
          component: ReportDetail
        },
        {
          path: 'usermanager',
          name: 'UserManager',
          component: UserManager
        },
        {
          path: 'project',
          name: 'Project',
          component: Project
        }
      ]
    },
    {
      path:"/login",
      name: 'login',
      component: login
    },
    
  ]
})
